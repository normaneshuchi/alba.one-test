import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    ToastAndroid
} from "react-native";

import t from 'tcomb-form-native';

const User = t.struct({
  email: t.String,
  password: t.String,
  terms: t.Boolean
});

const options = {
    fields: {
        terms: {
            label: "By selecting this, you agree to our terms and conditions"
        },
        email: {
            error: 'E-mail or no entry'
          },
          password: {
            password: true,
            secureTextEntry: true,
            error: 'Sorry, passwords needed'
        },
    }
}

const Form = t.form.Form;



class LoginScreen extends React.Component {

    static navigationOptions = {
        title: 'Login',
        headerStyle: {
            backgroundColor: '#25aae1',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
    }

    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
          };    

    }

    
    

    onPress = () => {
 
        var value = this.refs.form.getValue();
        if (value) { 
          console.log(value);

          this.setState({
              email: value.email,
              password: value.password
          });
          this.login();
        }
      }

    login = () => {
        fetch('http://51.15.54.38:3000/login', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
            })
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if (responseJson.status == 200){
          console.log(responseJson.status);
          ToastAndroid.show('Success!!!!', ToastAndroid.LONG);
        this.props.navigation.replace('Landing')

        }
        if (responseJson.message == "an error occured"){
          ToastAndroid.show('We have an error. Try again', ToastAndroid.SHORT);
        }
    }).catch((error) => {
        console.log(error);
        ToastAndroid.show('We have an error. Try again', ToastAndroid.SHORT);
    })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.titleBox}>
                    <Text style={styles.titleText}>Welcome to the test app</Text>
                </View>
                <View style={styles.inputBox}>
                    <Form ref="form" options={options} type={User} />
                    <TouchableHighlight style={styles.button} onPress={this.onPress} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}
export default LoginScreen;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#25aae1'
    },
    titleBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleText : {
        fontSize: 30,
        color: 'white'
    },
    inputBox : {
        flex: 3,
        margin: 0,
        padding: 10,
        justifyContent: 'center',
        backgroundColor: 'beige'
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
      },
      button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
      }
 
});

