import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight
} from "react-native";

class LandingScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            longitude: null,
            latitude: null,
            error: null
        }

        this.logout = this.logout.bind(this);

    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
              loading: false
            });
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
      }

    logout() {
        this.props.popToTop();
    }

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: '#25aae1',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
      }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.titleText}>Here is your Landing Page</Text>
                <Text style={styles.locationText}>Your Current Location is Longitude: {this.state.longitude} Latitude: {this.state.latitude} </Text>
                    <TouchableHighlight style={styles.button} onPress={() => this.props.navigation.popToTop()} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>Log Out</Text>
                </TouchableHighlight>
            </View>
        );
    }
}
export default LandingScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#25aae1',
        padding: 40
    },
    titleText: {
        fontSize: 25,
        color: 'yellow',
        textAlign: 'center'
    },
    locationText: {
        fontSize: 20,
        color: 'yellow',
        margin: 20,
        textAlign: 'justify'
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
      },
      button: {
        height: 36,
        backgroundColor: 'gold',
        borderColor: 'gold',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
      },
});