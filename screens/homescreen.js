import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Button,
    ToastAndroid,
    ScrollView,
    TouchableHighlight
} from "react-native";

import t from 'tcomb-form-native';
import axios from 'axios';

var Gender = t.enums({
    M: 'Male',
    F: 'Female'
  });

const User = t.struct({
    name: t.String,
    gender: Gender,
    phone: t.Number,
    businessName: t.String,
    businessAddress: t.String,
    email: t.String,
    password: t.String,
  });

  const options = {
    fields: {
        terms: {
            label: "By selecting this, you agree to our terms and conditions"
        },
        email: {
            error: 'E-mail or no entry'
          },
          password: {
            password: true,
            secureTextEntry: true,
            error: 'Sorry, passwords needed'
        },
    }
}

  const Form = t.form.Form;

class HomeScreen extends Component {

    static navigationOptions = {
        title: 'Home',
        headerStyle: {
            backgroundColor: '#25aae1',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
      };

      constructor(props) {
        super(props);
    
        this.state = {
          latitude: null,
          longitude: null,
          error: null,
          physicalAddress: null,
          loading: true,
          name: null,
            gender: null,
            phone: null,
            businessName: null,
            businessAddress: null,
            email: null,
            password: null
        };

        this.login = this.login.bind(this);
      }

      componentDidMount() {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
              loading: false
            });
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
      }

      submitCheck = () => {
 
        var value = this.refs.form.getValue();
        if (value) { 
          console.log(value.name);

          this.setState({
              email: value.email,
              password: value.password,
              name: value.name,
                gender: value.gender,
                phone: value.phone,
                businessName: value.businessName,
                businessAddress: value.businessAddress,
          });
          this.submit();
        }
      }

      submit() {
        fetch('http://51.15.54.38:3000/user', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                name: this.state.name,
                gender: this.state.gender,
                phone: this.state.phone,
                businessName: this.state.businessName,
                businessAddress: this.state.businessAddress,
                longitude: this.state.longitude,
                latitude: this.state.latitude
            })
      }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if (responseJson.status == 200){
            console.log(responseJson.status);
            ToastAndroid.show('Success!!!!', ToastAndroid.LONG);
            this.login();
          }
          if (responseJson.message == "an error occured"){
            ToastAndroid.show('We have an error. Try again', ToastAndroid.SHORT);
          }
      }).catch((error) => {
          console.log(error);
          ToastAndroid.show('We have an error. Try again', ToastAndroid.SHORT);
      })
    }

    login(){
        this.props.navigation.navigate('Login');
    }

    componentWillUnmount() {
      navigator.geolocation.clearWatch(this.watchId);
    }
    
    render() {
        return (
            <ScrollView>
            <View style={styles.container}>
                <Text style={styles.heading}>Kindly enter your details</Text>
                <Form ref="form" type={User} options={options} />
                <Text style={styles.locationText} loading={false}>Latitude:  {this.state.latitude}</Text>
                <Text style={styles.locationText} loading={false}>Longitude: {this.state.longitude}</Text>
                <TouchableHighlight style={styles.button} onPress={this.submitCheck} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>submit</Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button2} onPress={this.login} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>Login Instead</Text>
                </TouchableHighlight>
                
                {/* <Button title="go back to login screen" onPress={() => this.props.navigation.goBack()} />
                <Button title="go back to login screen" onPress={() => this.props.navigation.popToTop()} /> */}
            </View>
            </ScrollView>
        );
    }
}
export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        paddingTop: 25,
        padding: 10,
        // justifyContent: 'center',
        backgroundColor: 'white'
    },
    heading: {
        fontSize: 20,
        fontWeight: '800',
        alignSelf: 'center'
        
    },
    titleText: {
      fontSize: 25,
      color: 'gray',
      textAlign: 'center'
  },
  locationText: {
      fontSize: 20,
      color: 'gray',
      margin: 20,
      textAlign: 'justify'
  },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
      },
      button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
      },
      button2: {
        height: 36,
        backgroundColor: 'purple',
        borderColor: 'purple',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
      }
});