/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createStackNavigator } from "react-navigation";
import  LoginScreen  from "./screens/loginscreen";
import  HomeScreen  from "./screens/homescreen";
import  LandingScreen  from "./screens/landing";





type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <NavController/>
    );
  }
}




const NavController = createStackNavigator({
  Login: LoginScreen,
  Home: HomeScreen,
  Landing: LandingScreen
},
{ initialRouteName: 'Home'})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#25aae2',
  },
  container1: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#33aae2',
  },
  welcome: {
    fontSize: 40,
    textAlign: 'center',
    color: '#ffffff',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
